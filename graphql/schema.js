const { gql } = require("apollo-server")

module.exports = gql`
	type Query {
		getUsers : [User]
        getUser(id: String): User
        getItems : [Item]
        getItem(id: String): Item
        getCategories: [Category]
		getCategory(id: String): Category
	}

	type User {
		id: ID!
		name: String!
        email: String!
        password: String!
        mobile_no: String!
	}

	type Item {
		id: ID!
        name: String!
        description: String!
        unitPrice: Number!
        category: [Category]
    }
    
    type Category {
        id: ID!
        name: String!
        description: String!
	}
`;
